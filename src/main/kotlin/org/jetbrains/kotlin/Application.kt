package org.jetbrains.kotlin.demo

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.scheduling.annotation.EnableScheduling
import twitter4j.Twitter
import twitter4j.Status
import twitter4j.StatusListener
import twitter4j.StatusDeletionNotice;
import twitter4j.conf.ConfigurationBuilder
import twitter4j.TwitterStream
import twitter4j.TwitterStreamFactory
import twitter4j.FilterQuery
//import JavaTestController::class.java
import twitter4j.StallWarning


@SpringBootApplication
@EnableScheduling
class Application
//class BatchKotlinExampleApplication

fun main(args: Array<String>) {
    SpringApplication.run(Application::class.java, *args)

    println("started!!!!!")
    //getTwitterStream()
     //SpringApplication.run(JavaTestController::class.java, args);
     //val jtc = JavaTestController::class


    
	//val twitterStream: TwitterStream = TwitterStreamFactory(configurationBuilder.build()).getInstance()

	val listener = object : StatusListener {


        override fun onStatus(status: Status) {
            System.out.println("@" + status.getUser().getScreenName() + " - " + status.getText())
        }

        override fun onDeletionNotice(statusDeletionNotice: StatusDeletionNotice) {
            System.out.println("Got a status deletion notice id:" + statusDeletionNotice.getStatusId())
        }

        override fun onTrackLimitationNotice(numberOfLimitedStatuses: Int) {
            System.out.println("Got track limitation notice:" + numberOfLimitedStatuses)
        }

        override fun onScrubGeo(userId: Long, upToStatusId: Long) {
            System.out.println("Got scrub_geo event userId:" + userId + " upToStatusId:" + upToStatusId)
        }

        override fun onException(ex: Exception) {
            ex.printStackTrace()
        }

        override fun onStallWarning(arg0: StallWarning) {
        }

    }

    //Twitter4JHelper.addStatusListener(twitterStream, listener)
	//emitter.setCancellable { twitterStream::shutdown }

	val fq: FilterQuery = FilterQuery()
    val keywords = arrayOf("France", "Germany")

    /*fq.track(keywords);

    twitterStream.addListener(listener);
    twitterStream.filter(fq); */


}