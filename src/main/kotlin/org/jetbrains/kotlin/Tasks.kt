package org.jetbrains.kotlin.demo

import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpEntity
import org.springframework.util.LinkedMultiValueMap
import org.springframework.util.MultiValueMap
import org.springframework.http.MediaType
import twitter4j.Twitter
import twitter4j.conf.ConfigurationBuilder
//import twitter4j.StatusListener
//import twitter4j.TwitterStream
import twitter4j.TwitterStreamFactory

@JsonIgnoreProperties(ignoreUnknown = true)
data class Value(var id: Long = 0, var quote: String = "")

@JsonIgnoreProperties(ignoreUnknown = true)
data class Quote(var type : String = "", var value : Value? = null)

@Component
public class ScheduledTasks {
    //private static final Logger logger = LoggerFactory.getLogger(ScheduledTasks.class);
    //private static final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("HH:mm:ss");

    
    //@Scheduled(fixedRate = 2000)
    fun scheduleTaskWithFixedRate() {

    	println("task")

    	//RestTemplate restTemplate = new RestTemplate();
        //Quote quote = restTemplate.getForObject("http://gturnquist-quoters.cfapps.io/api/random", Quote.class);

        val quote = RestTemplate().getForObject("https://trends.google.com/trends/api/explore", Quote::class.java)
        println(quote)

        //val headers: HttpHeaders = HttpHeaders()
		//headers.set("custom-header-key","custom-header-value");

		val map = LinkedMultiValueMap<String, String>()
	    map.add("grant_type", "password")
	    //map.add("username", username)
		//map.add("password", password)

		val headers = HttpHeaders()
	    headers.contentType = MediaType.APPLICATION_FORM_URLENCODED
	    //val encodedAuth = Base64Utils.encodeToString("".toByteArray(StandardCharsets.UTF_8))
		//headers["Authorization"] = "Basic "
	    
	    val entity = HttpEntity<MultiValueMap<String, String>>(map, headers)

		 //RestTemplate restTemplate = new RestTemplate();
		 //ResponseEntity<ResponseObj> responseObj = restTemplate.exchange("<end point url>", HttpMethod.GET,entity,ResponseObj.class);
		 //ResponseObj resObj = responseObj.getBody();

    }

    //@Scheduled(fixedRate = 2000)
    fun dailyTokenTradingVolumeFromCryptoCompare() {

    	println("task")

    	
    }

    //@Scheduled(fixedRate = 2000)
    fun dailyTwitterFollowerCount() {

    	println("task")

    	
    }


    //@Scheduled(fixedRate = 2000)
    fun dailyTwitterTweetRetweetCount() {

    	println("task")

    	
    }

    fun twitterStream(){
	    
	    val configurationBuilder: ConfigurationBuilder = ConfigurationBuilder();
	    configurationBuilder.setOAuthConsumerKey("XyAdAA7hO170G5mrDjanChxIk")
	                .setOAuthConsumerSecret("7MCy8f2XjTT1o9HQyf3RWdnZJVV1rr3zYoXFqdCnp0eoGvb8OF")
	                .setOAuthAccessToken("915693939763552256-Tb009xWt45QVzVpH70agwYOV5r5H9mg")
	                .setOAuthAccessTokenSecret("AeXcrUpDQdRCohC3xB9BMVHCAsk0QqJfxxn1nlkfeCC8I")

	    //val twitterStream: TwitterStream = TwitterStreamFactory(configurationBuilder.build()).getInstance()

	    //twitterStream.addListener(StatusListener () {
	    //  onStatus(status: Status) {
	    //     System.out.println(status.getText()) // print tweet text to console
	    //  }
	    //}

    }

    //@Scheduled(fixedRate = 2000)
    fun dailyEtherscanTransactions() {

    	//val response = http://api.etherscan.io/api?module=account&action=txlist&address=0xddbd2b932c763ba5b1b7ae3b362eac3e8d40121a&startblock=0&endblock=99999999&sort=asc&apikey=YourApiKeyToken

    	//println(response)

    	//println(RestTemplate().getForObject("https://api.etherscan.io/api?module=account&action=txlist&address=0xddbd2b932c763ba5b1b7ae3b362eac3e8d40121a&startblock=0&endblock=99999999&page=1&offset=10&sort=asc&apikey=YourApiKeyToken"))

    	
    }

    //public void scheduleTaskWithFixedDelay() {}

    //public void scheduleTaskWithInitialDelay() {}

    //public void scheduleTaskWithCronExpression() {}
}
