package org.jetbrains.kotlin.demo;

//import JavaTestController.*;

public class Greeting {

    private final long id;
    private final String content;

    //getTwitterStream twitterStream = new getTwitterStream();

    public Greeting(long id, String content) {
        this.id = id;
        this.content = content;
    }

    public long getId() {
        return id;
    }

    public String getContent() {
        return content;
    }
}